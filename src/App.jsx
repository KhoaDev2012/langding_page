import FeatureSection from "./components/FeatureSection";
import Feedback from "./components/Feedback";
import Footer from "./components/Footer";
import HeroSection from "./components/HeroSection";
import Navbar from "./components/Navbar";
import Pricing from "./components/Pricing";
import WorkFlow from "./components/WorkFlow";

export default function App() {
  return (
    <>
      <Navbar />
      <div className="max-w-7xl mx-auto pt-20 px-6">
        <HeroSection />
        <FeatureSection />
        <WorkFlow />
        <Pricing />
        <Feedback />
        <Footer />
      </div>
    </>
  );
}
